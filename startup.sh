#/bin/bash

if [ ! -f /var/lib/mysql/roadmap ]; then
    mysql_install_db

    /usr/bin/mysqld_safe &
    sleep 10s

    echo "GRANT ALL ON *.* TO django@'%' IDENTIFIED BY 'django' WITH GRANT OPTION; FLUSH PRIVILEGES" | mysql
    echo "CREATE DATABASE roadmap" | mysql

    killall mysqld
    sleep 10s
fi

/usr/bin/mysqld_safe &
