__author__ = 'tyrel'
import datetime
from django.conf import settings

def get_status(title, due_date):
    overdue = ""
    message = title
    if due_date:
        if due_date < datetime.date.today():
            overdue = " Overdue"
        message += " ({}{})".format(due_date.strftime(settings.DATE_FMT), overdue)
    return message

