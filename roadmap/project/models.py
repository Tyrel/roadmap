from django.db import models
import datetime
from django.conf import settings
from orderable.models import Orderable

STATUSES = (
    (0, 'Upcoming'),
    (1, 'Current'),
    (2, 'Backlog'),
    (3, 'Completed'),
)


class Project(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField(blank=True, null=True)
    status = models.IntegerField(choices=STATUSES, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    @property
    def furthest_complete(self):
        sorted_duedates = self.duedate_set.order_by("-title__sort_order")
        for duedate in sorted_duedates:
            if duedate.completed:
                return unicode(duedate)
        return None


class Category(Orderable):
    title = models.CharField(max_length=256)

    class Meta(Orderable.Meta):
        verbose_name_plural = "Categories"

    def __unicode__(self):
        return self.title


class DueDate(models.Model):
    title = models.ForeignKey("Category")
    due = models.DateField(blank=True, null=True)
    completed = models.BooleanField(default=False)
    project = models.ForeignKey("Project")

    @property
    def completed_overdue_date(self):
        if self.completed:
            return "Completed"
        if not self.due:
            return ""
        if self.due < datetime.date.today():
            return "Overdue on {}".format(self.due.strftime(settings.DATE_FMT))
        return self.due.strftime(settings.DATE_FMT)

    class Meta:
        verbose_name = "Due Date"
        verbose_name_plural = "Due Dates"

    def __unicode__(self):
        return unicode(self.title)