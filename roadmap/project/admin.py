from django.contrib import admin
from orderable.admin import OrderableAdmin

from project.models import Project, DueDate, Category
# Register your models here.

class DueDateInline(admin.TabularInline):
    model = DueDate
    extra = 1


class CategoryClass(OrderableAdmin):
    list_display = ('__unicode__', 'sort_order_display')

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title','description','status')
    fields = ['title','description','status']
    inlines = (DueDateInline,)


class DueDateClass(admin.ModelAdmin):
    list_display = ('__unicode__', 'project', 'completed_overdue_date')


admin.site.register(Project, ProjectAdmin)
admin.site.register(DueDate, DueDateClass)
admin.site.register(Category, CategoryClass)