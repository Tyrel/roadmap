__author__ = 'tyrel'
from rest_framework import serializers
from project.models import Project, Category, DueDate


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Project


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category


class DueDateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DueDate