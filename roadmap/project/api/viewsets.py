__author__ = 'tyrel'
from rest_framework import viewsets
from project.models import Project, Category, DueDate
from project.api.serializers import ProjectSerializer, CategorySerializer, DueDateSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class DueDateViewSet(viewsets.ModelViewSet):
    queryset = DueDate.objects.all()
    serializer_class = DueDateSerializer