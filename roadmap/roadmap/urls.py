from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from user_api_views import UserViewSet
from project.api.viewsets import ProjectViewSet, CategoryViewSet, DueDateViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'projects', ProjectViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'due_dates', DueDateViewSet)

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^api/', include(router.urls)),
                       url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
