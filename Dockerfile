FROM ubuntu

RUN apt-get update
RUN apt-get upgrade -y

# Requirements
RUN apt-get install -y python-dev
RUN apt-get install -y build-essential g++ git cmake zlib1g-dev libbz2-dev
RUN apt-get install -y libmysqlclient-dev

# Install Python Setuptools
RUN apt-get install -y python-setuptools

# Install pip
RUN easy_install pip



RUN apt-get -y install mysql-server


# Add and install Python modules
ADD roadmap/requirements.txt /roadmap/requirements.txt
RUN cd /roadmap; pip install -r requirements.txt

# Bundle app source
ADD roadmap /roadmap
ADD startup.sh /opt/startup.sh


# Change Mysql BindAddress
RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
RUN /bin/bash /opt/startup.sh
RUN cd /roadmap; python manage.py migrate

# Expose
EXPOSE  8000
EXPOSE  3306

# http://txt.fliglio.com/2013/11/creating-a-mysql-docker-container/
# http://blogs.aws.amazon.com/application-management/post/Tx1ZLAHMVBEDCOC/Dockerizing-a-Python-Web-App
